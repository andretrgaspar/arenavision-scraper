# arenavision-scraper

Arenavision scraper reads the content from http://arenavision.in/guide and parses the guide of Arenavision events and channels.

It exposes 2 REST APIs to retrieve the events details and the channels ids to use on Acestream.

A CRON job is executing every 2 hours to fetch content and update information of APIs. 

> Please do not "*attack*"  the servers since SERVER #1 is running in my old RASPBERRY PI 1 MODEL B+ =)

> **For now source-code is not public however feel free to use the APIs on your applications.**


**Table of Contents**

[TOC]


------------


## APIs
### Events
- **SERVER #1 URL:** GET http://notathomenow.ddns.net/php/arenavision.php?action=events
- **SERVER #2 URL:** GET http://fiptv.uphero.com/php/arenavision.php?action=events

Returns an array of events including the day, time (Portugal timezone), sport, competition, event name and a channel list, containing the channel id and country code.

**Example JSON**
```json
{  
   "updateDate":"2018-07-11T16:00:20.436Z",
   "events":[  
      {  
         "day":"11/07/2018",
         "time":"11:00",
         "sport":"CYCLING",
         "competition":"LE TOUR DE FRANCE",
         "event":"STAGE 5",
         "channels":{  
            "13":"SPA",
            "14":"SPA"
         }
      },
      {  
         "day":"11/07/2018",
         "time":"13:00",
         "sport":"TENNIS",
         "competition":"WIMBLEDON 2018",
         "event":"MEN'S QUARTERFINALS",
         "channels":{  
            "17":"SPA",
            "18":"SPA"
         }
      },
      {  
         "day":"11/07/2018",
         "time":"14:30",
         "sport":"SOCCER",
         "competition":"UEFA EUROPA LEAGUE",
         "event":"BANANTS - SARAJEVO",
         "channels":{  
            "19":"POR",
            "20":"POR"
         }
      }
   ]
}
```


### Channels
- **SERVER #1 URL:** GET http://notathomenow.ddns.net/php/arenavision.php?action=channels
- **SERVER #2 URL:** GET http://fiptv.uphero.com/php/arenavision.php?action=channels

Returns a an array of channels containing the channel id and the acestream link.

**Example JSON**
```json
[  
   {  
      "1":"acestream://9532ad01136f60426d4390e45114cd2c4d5da79e"
   },
   {  
      "2":"acestream://02f8a59dd859eb57793ff52c1946a41964837c6b"
   },
   {  
      "3":"acestream://e32b2bb54354cb03d594b52ad847d8eb9845fb72"
   },
   {  
      "4":"acestream://0b2250ee8996cb8c4fbffdb9f372658394905a5f"
   },
   {  
      "5":"acestream://b4e6df0079ea6d21a231cf7b02808c2387c965d5"
   },
   {  
      "6":"acestream://934eb0ebb6ddbbe877e75638568ba1c65cda5aaa"
   },
   {  
      "7":"acestream://83e95cf03d24b7e1601eaa91c296c30e795eed97"
   },
   {  
      "8":"acestream://5a3b3b3c3f91d0e64462a31b16acc8e9fc48dcec"
   },
   {  
      "9":"acestream://49ae7367753c9e906ba05ba23af1d8c7fdf8a20f"
   },
   {  
      "10":"acestream://2827908b2026bbb506c8a6ad698aa2c0d5c053ff"
   },
   {  
      "11":"acestream://2827908b2026bbb506c8a6ad698aa2c0d5c053ff"
   },
   {  
      "12":"acestream://378f59b4a88a8fe976c26b07bb220e9d5aa73a10"
   },
   {  
      "13":"acestream://1f6b76fb6678a05857c359f462df836872cd452f"
   },
   {  
      "14":"acestream://1f6b76fb6678a05857c359f462df836872cd452f"
   },
   {  
      "15":"acestream://3e57b7e5206d645949488550425ce6b36c66f545"
   },
   {  
      "16":"acestream://3e57b7e5206d645949488550425ce6b36c66f545"
   },
   {  
      "17":"acestream://3e54542bfaed903a4b646b41dd2d5767b2f2e926"
   },
   {  
      "18":"acestream://3e54542bfaed903a4b646b41dd2d5767b2f2e926"
   },
   {  
      "19":"acestream://ca59954d8ea107c5afc6e9b55677857a8c7445bb"
   },
   {  
      "20":"acestream://ca59954d8ea107c5afc6e9b55677857a8c7445bb"
   },
   {  
      "21":"acestream://3b4886c141c56d2ff075b6ae01fdbb79c4579a93"
   }
]
```

### Channel by Id
- **SERVER #1 URL:** GET http://notathomenow.ddns.net/php/arenavision.php?action=channels&channelId=20
- **SERVER #2 URL:** GET http://fiptv.uphero.com/php/arenavision.php?action=channels&channelId=20

Filters the channel by id and return the acestream link.

**Example JSON**
```json
{"20": "acestream://ca59954d8ea107c5afc6e9b55677857a8c7445bb"}
```

## Reference Links
- Arenavision website: http://arenavision.in/ (thanks!)
- Acestream System: http://www.acestream.org/


## Improvements
- Add extra query parameters in Events API to filter events by day, sport, event name or competition.
- Possibly others


## Disclaimer
We do not own any of this information and it is scraped from Arenavision website.
